import sys
import socket
import threading

# Lista de caracteres printeables basados en Hex por tener len=3 en su repr
HEX_FILTER = ''.join([(len(repr(chr(i))) == 3) and chr(i) or '.' for i in range(256)])

# Dumpeo del segmento + Hex + Printeable
def hexdump(src, length=16, show=True):
    if isinstance(src, bytes):
        src = src.decode()

    results = list()
    # range(start,stop,step)
    for i in range(0, len(src), length):
        # Pillamos un slice de tamaño length del src
        word = str(src[i:i+length])
        # Reemplazamos caracteres del src en base a HEX_FILTER
        printable = word.translate(HEX_FILTER)
        # Creamos lista tal que: por cada caracter, formatea(f'') su representacion decimal(ord(c)) en hexadecimal(X) con un total de dos caracteres(2) y añadiendo los 0's necesarios a la izquierda(0)
        hexa = ' '.join([f'{ord(c):02X}' for c in word])
        hexwidth = length*3
        # Añadimos a la lista linea por linea: 
            # i(nº de iteracion) en formato hexa con 4 caracteres y añadiendo 0's
            # Formatea el espaciado de hexa en base a hexawidth
            # printable
        results.append(f'{i:04x}  {hexa:<{hexwidth}}  {printable}')
    if show:
        for line in results:
            print(line)
    else:
        return results

# Recibe la conxión y devuelve un buffer
def receive_from(connection):
    buffer = b''
    connection.settimeout(5)
    try:
        while True:
            data = connection.recv(4096)
            if not data:
                break
            buffer += data
    except Exception as e:
        pass
    return buffer


# Modificaciones a peticiones y respuestas
def request_handler(buffer):
    # perform packet modifications
    return buffer

def response_handler(buffer):
    # perform packet modifications
    return buffer


# Gestion de conexiones entre cliente y servidor
def proxy_handler(client_socket, client_addr, remote_host, remote_port, receive_first):
    try:
        remote_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        remote_socket.connect((remote_host, remote_port))
    except Exception as e:
        print(f'[*] Unable to connect:\n\n{e}')

    remote_buffer = b''

    if receive_first:
        remote_buffer = receive_from(remote_socket)
        hexdump(remote_buffer)


    remote_buffer = response_handler(remote_buffer)
    if len(remote_buffer):
        print("[<==] Sending %d bytes to %s:%d." % (len(remote_buffer),client_addr[0],client_addr[1]))
        client_socket.send(remote_buffer)


    while True:
        local_buffer = receive_from(client_socket)
        if len(local_buffer):
            line = "[==>]Received %d bytes from %s:%d." % (len(local_buffer),client_addr[0],client_addr[1])
            print(line)
            hexdump(local_buffer)

            local_buffer = request_handler(local_buffer)
            remote_socket.send(local_buffer)
            print("[==>] Sent to %s:%d." % remote_host,remote_port)

        remote_buffer = receive_from(remote_socket)
        if len(remote_buffer):
            print("[<==] Received %d bytes from %s:%d." % (len(remote_buffer),remote_host,remote_port))
            hexdump(remote_buffer)

            remote_buffer = response_handler(remote_buffer)
            client_socket.send(remote_buffer)
            print("[<==] Sent to %s:%d." % (client_addr[0],client_addr[1]))



        if not len(local_buffer) or not len(remote_buffer):
            client_socket.close()
            remote_socket.close()
            print("[*] No more data. Closing connections.")
            break


# Loop del server que redirige a proxy_handler
def server_loop(local_host, local_port, remote_host, remote_port, receive_first):
    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        server.bind((local_host, local_port))
    except Exception as e:
        print('problem on bind: %r' % e)
   
        print("[!!] Failed to listen on %s:%s" % (local_host, local_port))
        print("[!!] Check for other listening sockets or correct permissions.")
        sys.exit(0)

    print("[*] Listening on %s:%d" % (local_host, local_port))
    server.listen(5)
    while True:
        client_socket, client_addr = server.accept()
        # print out the local connection information
        line = "> Received incoming connection from %s:%d" % (client_addr[0], client_addr[1])
        print(line)
        # start a thread to talk to the remote host
        proxy_thread = threading.Thread(target=proxy_handler,args=(client_socket, client_addr, remote_host, remote_port, receive_first))
        proxy_thread.start()


def main():
    if len(sys.argv[1:]) != 5:
        print("Usage: ./proxy.py [localhost] [localport] [remotehost] [remoteport] [receive_first]")
        print("Example: ./proxy.py 127.0.0.1 9000 10.12.132.1 9000 True")
        sys.exit()
    
    local_host = sys.argv[1]
    local_port = int(sys.argv[2])

    remote_host = sys.argv[3]
    remote_port = int(sys.argv[4])
    
    receive_first = sys.argv[5]

    if "True" in receive_first:
        receive_first = True
    else:
        receive_first = False

    server_loop(local_host, local_port,remote_host, remote_port, receive_first)

if __name__ == '__main__':
    main()
