from scapy.all import sniff

def packet_callback(packet):  
	# packet.show bien util para mostrar el paquete entero
	print(packet.show())  

def main():  
	# Pilla 1 paquete y le aplica packet_callback()
	sniff(prn=packet_callback, count=1)

if __name__ == '__main__':  
	main()