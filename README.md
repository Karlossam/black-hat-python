# Black Hat Python

Tools developed during the reading of the fantastic book "Black Hat Python v2".

The folders represent each chapter of the book, trying to collect all the scripts that the author shows us with the idea of improving them

Maybe in the future I will update each script with its own README.md where I will explain in detail the whole script and usage
